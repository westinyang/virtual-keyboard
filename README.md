# VirtualKeyboard

<img src="entry/src/main/resources/base/media/icon.png" width="128px" />

## 项目介绍

支持中文输入的虚拟键盘应用，使用Web技术混合开发，对于现阶段OHOS系统键盘不支持中文输入的临时方案

- 基于 https://github.com/mowatermelon/visualKeyboard 进行二次开发，尊重原项目作者的开源贡献，特此声明
- 接入 OpenHarmony API 实现与系统能力的交互

> 目前仅在`一加6T`上测试过，其他OpenHarmony设备未测试，界面布局兼容性未知

## 修改说明

> 后续修改说明将不在这里补充，具体请查看commit中的信息

- 虚拟键盘配色和细节调整，元素间隔和对齐优化
- 输入悬浮选择框定位优化，精确计算元素位置
- 初始化校验规则修改，兼容textarea元素
- 阻止系统自带键盘弹出
- 新增自制OHOS输入法主题
- 新增复制和重置功能
- 复制和提示依赖OHOS的API
    - 接入 @ohos.web.webview (Web组件) 注入JavaScript扩展对象用于交互调用
    - 接入 @ohos.pasteboard (剪贴板)
    - 接入 @ohos.promptAction (弹窗)

## 开发环境

> [Full-SDK编译和替换指南](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Release/zh-cn/application-dev/quick-start/full-sdk-compile-guide.md)

- DevEco Studio 3.1 Release
- SDK API9 3.2.12.5 Release (Full-SDK)

## 截图预览

<img src="screenshot/03.jpg" width="300px" />&emsp;<img src="screenshot/04.jpg" width="300px" />

## 视频演示

https://www.bilibili.com/video/BV1Tc41157Ce

## 许可声明

- 应用图标出处：https://www.iconarchive.com/show/papirus-apps-icons-by-papirus-team/preferences-desktop-keyboard-shortcuts-icon.html